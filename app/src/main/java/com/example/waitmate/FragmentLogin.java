package com.example.waitmate;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;

public class FragmentLogin extends Fragment {

    Button b_login, b_register;
    EditText username, password;
    FragmentCallback fragmentCallback;
    int counter = 3;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d("Activity Lifecycle","onCreateView() Called");
        View v = inflater.inflate(R.layout.login_fragment,container,false);

        b_login = (Button) v.findViewById(R.id.login);
        b_register = (Button) v.findViewById(R.id.register);
        username = (EditText)v.findViewById(R.id.username);
        password = (EditText)v.findViewById(R.id.password);

        b_login.setOnClickListener(v1 -> {
            if (((ActivityLogin)requireActivity()).isOnline()) {
                FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
                DatabaseReference mReference = mDatabase.getReference("User");
                SharedPreferences sharedPreferences = requireContext().getSharedPreferences("UserID", Context.MODE_PRIVATE);
                String userName = username.getText().toString();
                String passWord = password.getText().toString();
                mReference.child(userName).addListenerForSingleValueEvent(
                        new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot.exists() && Objects.equals(dataSnapshot.child("password").getValue(String.class), passWord)) {
                                    // User exists and correct password
                                    SharedPreferences.Editor myEdits = sharedPreferences.edit();
                                    myEdits.putString("UID", userName);
                                    myEdits.apply();
                                    startActivity(new Intent(FragmentLogin.this.getActivity(), ActivityMainList.class));
                                } else {
                                    Toast.makeText(getActivity(), getResources().getString(R.string.incorrect_cred),Toast.LENGTH_SHORT).show();
                                    counter--;
                                    if (counter == 0) {
                                        b_login.setEnabled(false);
                                    }
                                }
                            }
                            @Override
                            public void onCancelled (@NonNull DatabaseError databaseError){
                                Toast.makeText(getActivity(), getResources().getString(R.string.failed_data) + databaseError, Toast.LENGTH_SHORT).show();
                            }
                        }
                );
            } else {
                Toast.makeText(getActivity(), getResources().getString(R.string.no_network), Toast.LENGTH_SHORT).show();
            }
        });

       b_register.setOnClickListener(v12 -> {
           if (fragmentCallback != null){
               fragmentCallback.replaceFragment();
           }
       });

        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d("Activity Lifecycle","onDestroyView() Called");

    }

    public void set_fragment_callback(FragmentCallback fragmentCallback){
        this.fragmentCallback = fragmentCallback;
    }
}
