package com.example.waitmate;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class FragmentRegister extends Fragment {
    Button b_submit;
    EditText username, password, zipCode;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d("Activity Lifecycle","onCreateView() Called");
        View v = inflater.inflate(R.layout.register_fragment,container,false);

        b_submit = (Button)v.findViewById(R.id.submit);
        username = (EditText)v.findViewById(R.id.username);
        password = (EditText)v.findViewById(R.id.password);
        zipCode = (EditText)v.findViewById(R.id.zipCode);

        b_submit.setOnClickListener(v1 -> {
            if (((ActivityLogin)requireActivity()).isOnline()) {
                if (!username.getText().toString().isEmpty() && !password.getText().toString().isEmpty() && !zipCode.getText().toString().isEmpty()) {
                    final String userName = username.getText().toString();
                    FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
                    DatabaseReference mReference = mDatabase.getReference("User");
                    mReference.child(userName).addListenerForSingleValueEvent(
                            new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    if (dataSnapshot.exists()) {
                                        // User Exists
                                        Toast.makeText(getActivity(), getResources().getString(R.string.user_exists), Toast.LENGTH_SHORT).show();
                                    } else {
                                        // Accepted number of  digits in a ZIP Code is 5
                                        if (zipCode.getText().toString().length() == 5) {
                                            // Valid inputs, registering new user
                                            User user = new User(password.getText().toString(), zipCode.getText().toString());
                                            mReference.child(userName).setValue(user);
                                            Toast.makeText(getActivity(), getResources().getString(R.string.account_create), Toast.LENGTH_SHORT).show();
                                            requireActivity().getSupportFragmentManager().popBackStack();
                                        } else {
                                            // Invalid zip code
                                            Toast.makeText(getActivity(), getResources().getString(R.string.invalid_zip), Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }
                                @Override
                                public void onCancelled (@NonNull DatabaseError databaseError){
                                    Toast.makeText(getActivity(), getResources().getString(R.string.fail_register) + databaseError, Toast.LENGTH_SHORT).show();
                                }
                            }

                    );
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.blank_fields),Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getActivity(), getResources().getString(R.string.no_network), Toast.LENGTH_SHORT).show();
            }
        });

        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d("Activity Lifecycle","onDestroyView() Called");
    }
}
