package com.example.waitmate;

public class Restaurant {
    private String resName;
    private int waitTime;
    private double rating;

    public Restaurant() {
    }

    public Restaurant(String resName, int waitTime, double rating) {
        this.resName = resName;
        this.waitTime = waitTime;
        this.rating = rating;
    }

    public String getResName() {
        return resName;
    }

    public void setResName(String resName) {
        this.resName = resName;
    }

    public int getWaitTime() {
        return waitTime;
    }

    public void setWaitTime(int waitTime) {
        this.waitTime = waitTime;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }
}
