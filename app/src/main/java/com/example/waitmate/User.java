package com.example.waitmate;

public class User {
    private String password;
    private String location;

    public User() {
    }

    public User(String password, String location) {
        this.password = password;
        this.location = location;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
