package com.example.waitmate;

public class Review {
    private String text;
    private String date;
    private int rating;

    public Review() {
    }

    public Review(String text, String date, int rating) {
        this.text = text;
        this.date = date;
        this.rating = rating;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

}
