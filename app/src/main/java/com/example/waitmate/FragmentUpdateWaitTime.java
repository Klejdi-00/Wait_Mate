package com.example.waitmate;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class FragmentUpdateWaitTime extends DialogFragment {

    Button wait_time_update;
    TextView actual_wait_time;
    EditText update_wait_et;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d("Activity Lifecycle", "onCreateView() Called");
        View v = inflater.inflate(R.layout.fragment_update_wait_time, container, false);

        wait_time_update = (Button) v.findViewById(R.id.wait_time_update);
        update_wait_et = (EditText) v.findViewById(R.id.update_wait_et);
        actual_wait_time = (TextView) v.findViewById(R.id.actual_wait_time);


        return v;
    }

    static FragmentUpdateWaitTime newInstance(int waitTime, String address) {
        FragmentUpdateWaitTime f = new FragmentUpdateWaitTime();
        Bundle args = new Bundle();
        args.putInt("waitTime", waitTime);
        args.putString("address", address);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.AppTheme_FullScreenDialog);
    }

    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
            dialog.getWindow().setWindowAnimations(R.style.Fade);
        }
        assert getArguments() != null;
        String str = getArguments().getInt("waitTime") + " " + getResources().getString(R.string.minutes);
        actual_wait_time.setText(str);

        String address = getArguments().getString("address");
        wait_time_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((ActivityMainList)requireActivity()).isOnline()) {
                    FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
                    DatabaseReference mReference = mDatabase.getReference("Restaurant/" + address +"/waitTime");
                    if (update_wait_et.getText().toString().length() > 0) {
                        mReference.setValue(Integer.parseInt(update_wait_et.getText().toString()));
                        Toast.makeText(getActivity(), getResources().getString(R.string.wait_update), Toast.LENGTH_SHORT).show();
                        if (((ActivityMainList)requireActivity()).isGPSEnabled()) {
                            Intent intent = new Intent(getContext(), ActivityMainList.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            dismiss();
                        } else {
                            Toast.makeText(getActivity(), getResources().getString(R.string.enable_gps), Toast.LENGTH_SHORT).show();
                        }
                        dismiss();
                    } else {
                        Toast.makeText(getActivity(), getResources().getString(R.string.no_val), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.no_network), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}