package com.example.waitmate;

import static com.example.waitmate.BuildConfig.MAPS_API_KEY;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.Priority;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Objects;

public class ActivityMainList extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerViewAdapter recyclerViewAdapter;
    ArrayList<String> rowsArrayList = new ArrayList<>();
    boolean isLoading = false;
    Button settings;
    private double latitude;
    private double longitude;
    private static String next_page_token = null;
    private LocationRequest locationRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("Activity Lifecycle","onCreate() Called");
        Objects.requireNonNull(getSupportActionBar()).hide();
        setContentView(R.layout.activity_mainlist);

        //Finishing ActivityLogin
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("com.package.ACTION_LOGIN");
        sendBroadcast(broadcastIntent);

        recyclerView = findViewById(R.id.recyclerView);

        // Get location and show the restaurant list
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(Priority.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(5000);
        locationRequest.setFastestInterval(2000);
        getLocationAndShowRestaurants();

        // Open settings fragment
        settings = (Button)findViewById(R.id.settings);
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment dialog = new FragmentSettings();
                dialog.show(getSupportFragmentManager(),"Settings");
            }
        });


        // Finishes activity when user logs out
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.package.ACTION_LOGOUT");
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d("onReceive","Logout in progress");
                finish();
            }
        }, intentFilter);
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        Log.d("Activity Lifecycle","onDestroy() Called");
        finish();
    }

    @Override
    public void onPause(){
        super.onPause();
        overridePendingTransition(0, 0);
    }

    @Override
    public void onResume(){
        super.onResume();
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1){
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
                if (isGPSEnabled()) {
                    getLocationAndShowRestaurants();
                } else {
                    turnOnGPS();
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2) {
            if (resultCode == Activity.RESULT_OK) {
                getLocationAndShowRestaurants();
            }
        }
    }

    public boolean isOnline() {
        Runtime runtime = Runtime.getRuntime();
        try {
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int     exitValue = ipProcess.waitFor();
            return (exitValue == 0);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void getLocationAndShowRestaurants() {
        rowsArrayList.clear();
            if (ActivityCompat.checkSelfPermission(ActivityMainList.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                if (isGPSEnabled()) {
                    if (isOnline()) {
                        Toast.makeText(ActivityMainList.this, getResources().getString(R.string.loading_resturant), Toast.LENGTH_LONG).show();
                        LocationServices.getFusedLocationProviderClient(ActivityMainList.this)
                                .requestLocationUpdates(locationRequest, new LocationCallback() {
                                    @Override
                                    public void onLocationResult(@NonNull LocationResult locationResult) {
                                        super.onLocationResult(locationResult);
                                        LocationServices.getFusedLocationProviderClient(ActivityMainList.this)
                                                .removeLocationUpdates(this);
                                        if (locationResult.getLocations().size() > 0) {
                                            int index = locationResult.getLocations().size() - 1;
                                            latitude = locationResult.getLocations().get(index).getLatitude();
                                            longitude = locationResult.getLocations().get(index).getLongitude();

                                            // Show the restaurants list
                                            populateData();
                                            initAdapter();
                                            initScrollListener();
                                        }
                                    }
                                }, Looper.getMainLooper());
                    } else {
                        Toast.makeText(ActivityMainList.this, getResources().getString(R.string.no_network), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    turnOnGPS();
                }
            } else {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            }
        }

    private void turnOnGPS() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        Task<LocationSettingsResponse> result = LocationServices.getSettingsClient(getApplicationContext())
                .checkLocationSettings(builder.build());

        result.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
                try {
                    LocationSettingsResponse response = task.getResult(ApiException.class);
                    Toast.makeText(ActivityMainList.this, getResources().getString(R.string.gps_on), Toast.LENGTH_SHORT).show();

                } catch (ApiException e) {

                    switch (e.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:

                            try {
                                ResolvableApiException resolvableApiException = (ResolvableApiException) e;
                                resolvableApiException.startResolutionForResult(ActivityMainList.this, 2);
                            } catch (IntentSender.SendIntentException ex) {
                                ex.printStackTrace();
                            }
                            break;

                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            //Device does not have location
                            break;
                    }
                }
            }
        });

    }

    public boolean isGPSEnabled() {
        LocationManager locationManager;
        boolean isEnabled;
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        isEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        return isEnabled;
    }


    private void populateData() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        SparseArray<Entry> resMap = new SparseArray<>();
        HttpURLConnection conn = null;
        BufferedReader reader;
        String line;
        StringBuilder responseContent = new StringBuilder();

        // URL for API request (restaurants within 5000m of current locations)
        String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?" +
                "radius=" + 5000 +
                "&location=" + latitude + "," + longitude +
                "&type=" + "restaurant" +
                "&key=" + MAPS_API_KEY;

        // Getting the JSON tree as a StringBuilder
        responseContent = performRequest(url);
        try {
            // Getting a map with restaurants' names and addresses
            resMap = parse(responseContent.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (resMap.size() > 0) {
            char separator = ((char)007);
            for (int i = 0; i < resMap.size(); i++) {
                Entry entry = resMap.get(i);
                rowsArrayList.add(entry.name + separator + entry.address);
            }
        }

        addToDatabase(resMap);
    }

    // Checks if restaurants are already in the database and adds them if not
    private void addToDatabase(SparseArray<Entry> resMap) {
        FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
        DatabaseReference mReference = mDatabase.getReference("Restaurant");
            for (int i = 0; i < resMap.size(); i++) {
                Entry entry = resMap.get(i);

                // Firebase doesn't use .,#,$,[,] in keys
                if (entry.address.contains("#")) entry.address = (entry.address.replace("#","Nº"));
                entry.address = (entry.address.replace(".",""));
                entry.address = (entry.address.replace("$",""));
                entry.address = (entry.address.replace("[",""));
                entry.address = (entry.address.replace("]",""));

                mReference.child(entry.address).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if (!snapshot.exists()) {
                            Restaurant restaurant = new Restaurant(entry.name, 0, 1);
                            mReference.child(entry.address).setValue(restaurant);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                        Toast.makeText(ActivityMainList.this, getResources().getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                    }
                });
            }
    }

    // Sends a request to get nearby restaurants to the Places API
    private static StringBuilder performRequest(String url) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        HttpURLConnection conn = null;
        BufferedReader reader;
        String line;
        StringBuilder responseContent = new StringBuilder();

        try {
            URL nearbySearch = new URL(url);
            conn = (HttpURLConnection) nearbySearch.openConnection();
            conn.setRequestMethod("GET");
            conn.setConnectTimeout(5000);
            conn.setReadTimeout(5000);

            // Test if the response from the server is successful
            int status = conn.getResponseCode();
            if (status >= 300) {
                reader = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
            } else {
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            }
            line = reader.readLine();
            while (line != null) {
                responseContent.append(line);
                line = reader.readLine();
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (conn != null)  conn.disconnect();
        }
        return responseContent;
    }

    public class Entry {
        public String name;
        public String address;
    }

    // Parses the JSON tree returned by nearby search
    private SparseArray<Entry> parse(String responseBody) throws JSONException {
        SparseArray<Entry> resMap = new SparseArray<>();
        JSONObject query = new JSONObject(responseBody);
        if (query.has("next_page_token")) {
            next_page_token = query.getString("next_page_token");
        } else {
            next_page_token = null;
        }
        JSONArray results = query.getJSONArray("results");
        for (int i = 0 ; i < results.length(); i++) {
            JSONObject restaurant = results.getJSONObject(i);
            Entry e = new Entry();
            e.name = restaurant.getString("name");
            e.address = restaurant.getString("vicinity");
            resMap.append(i, e);
        }
        return resMap;
    }

    private void initAdapter() {
        recyclerViewAdapter = new RecyclerViewAdapter(rowsArrayList, ActivityMainList.this);
        recyclerView.setAdapter(recyclerViewAdapter);
    }

    private void initScrollListener() {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == rowsArrayList.size() - 1) {
                        loadMore();
                        isLoading = true;
                    }
                }
            }
        });
    }

    private void loadMore() {
        if (isOnline()) {
            if (next_page_token != null) {
                rowsArrayList.add(null);
                recyclerViewAdapter.notifyItemInserted(rowsArrayList.size() - 1);

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void run() {
                        rowsArrayList.remove(rowsArrayList.size() - 1);
                        int scrollPosition = rowsArrayList.size();
                        recyclerViewAdapter.notifyItemRemoved(scrollPosition);
                        String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?" +
                                "&key=" + MAPS_API_KEY +
                                "&pagetoken=" + next_page_token;

                        StringBuilder responseContent = performRequest(url);
                        SparseArray<Entry> resMap = new SparseArray<>();
                        try {
                            resMap = parse(responseContent.toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        addToDatabase(resMap);

                        if (resMap.size() > 0) {
                            char separator = ((char) 007);
                            for (int i = 0; i < resMap.size(); i++) {
                                Entry e = resMap.get(i);
                                rowsArrayList.add(e.name + separator + e.address);
                            }
                        }
                        recyclerViewAdapter.notifyDataSetChanged();
                        isLoading = false;
                    }
                }, 2000);
            } else {
                Toast.makeText(ActivityMainList.this, getResources().getString(R.string.no_results), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(ActivityMainList.this, getResources().getString(R.string.no_network), Toast.LENGTH_SHORT).show();
        }
    }
}