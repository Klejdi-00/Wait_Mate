package com.example.waitmate;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import java.io.IOException;
import java.util.Objects;

public class ActivityLogin extends AppCompatActivity implements FragmentCallback {
    Fragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("Activity Lifecycle","onCreate() Called");
        Objects.requireNonNull(getSupportActionBar()).hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        addFragment();

        // Finishes activity when user logs out
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.package.ACTION_LOGOUT");
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d("onReceive","Logout in progress");
                finish();
            }
        }, intentFilter);
        intentFilter.addAction("com.package.ACTION_LOGIN");
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d("onReceive","Logout in progress");
                finish();
            }
        }, intentFilter);
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        Log.d("Activity Lifecycle","onDestroy() Called");
        finish();
    }

    public void addFragment(){
        FragmentLogin fragment = new FragmentLogin();
        fragment.set_fragment_callback(this);
        getSupportFragmentManager().beginTransaction()
        .setCustomAnimations(
                R.anim.slide_in,  // enter
                R.anim.fade_out,  // exit
                R.anim.fade_in,   // popEnter
                R.anim.slide_out  // popExit
        )
        .add(R.id.fragmentContainer, fragment)
        .commit();
    }

    @Override
    public void replaceFragment(){
        fragment = new FragmentRegister();
        getSupportFragmentManager().beginTransaction()
        .setCustomAnimations(
                R.anim.slide_in,  // enter
                R.anim.fade_out,  // exit
                R.anim.fade_in,   // popEnter
                R.anim.slide_out  // popExit
        )
        .addToBackStack(null)
        .replace(R.id.fragmentContainer, fragment)
        .commit();
    }

    public boolean isOnline() {
        Runtime runtime = Runtime.getRuntime();
        try {
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int     exitValue = ipProcess.waitFor();
            return (exitValue == 0);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }
}