package com.example.waitmate;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class FragmentSettings extends DialogFragment {

    Button log_out, delete_account, update_location;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d("Activity Lifecycle","onCreateView() Called");
        View v = inflater.inflate(R.layout.settings_fragment,container,false);

        log_out = v.findViewById(R.id.log_out);
        delete_account = v.findViewById(R.id.delete_account);
        update_location = v.findViewById(R.id.update_location);

        log_out.setOnClickListener(v1 -> {
            //Finish all activities and log user out
            Intent broadcastIntent = new Intent();
            broadcastIntent.setAction("com.package.ACTION_LOGOUT");
            requireActivity().sendBroadcast(broadcastIntent);
            Intent intent = new Intent(FragmentSettings.this.getActivity(), ActivityLogin.class);
            startActivity(intent);
        });

       delete_account.setOnClickListener(v12 -> {
           if (((ActivityMainList)requireActivity()).isOnline()) {
               FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
               DatabaseReference mReference = mDatabase.getReference("User");

               SharedPreferences sharedPref = requireContext().getSharedPreferences("UserID", Context.MODE_PRIVATE);
               String userName = sharedPref.getString("UID", "");

               mReference.child(userName).addListenerForSingleValueEvent(new ValueEventListener() {
                   @Override
                   public void onDataChange(@NonNull DataSnapshot snapshot) {
                       //Another dialog to prevent mis-click
                       DialogInterface.OnClickListener dialogClickListener = (dialog, which) -> {
                           switch (which){
                               case DialogInterface.BUTTON_POSITIVE:
                                   //Search for any instances of the username in the database.
                                   for (DataSnapshot userSnapshot: snapshot.getChildren()) {
                                       userSnapshot.getRef().removeValue();
                                   }

                                   //Finish all activities and log user out
                                   Intent broadcastIntent = new Intent();
                                   broadcastIntent.setAction("com.package.ACTION_LOGOUT");
                                   requireActivity().sendBroadcast(broadcastIntent);
                                   Intent intent = new Intent(FragmentSettings.this.getActivity(), ActivityLogin.class);
                                   startActivity(intent);
                                   Toast.makeText(getActivity(), getResources().getString(R.string.account_delete), Toast.LENGTH_SHORT).show();
                                   break;

                               case DialogInterface.BUTTON_NEGATIVE:
                                   break;
                           }
                       };

                       AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
                       builder.setMessage(getResources().getString(R.string.sure_delete)).setPositiveButton(getResources().getString(R.string.yes), dialogClickListener)
                               .setNegativeButton(getResources().getString(R.string.no), dialogClickListener).show();
                   }

                   @Override
                   public void onCancelled(@NonNull DatabaseError databaseError) {
                       Toast.makeText(getActivity(), getResources().getString(R.string.failed_data) + databaseError, Toast.LENGTH_SHORT).show();
                   }
               });
           } else {
               Toast.makeText(getActivity(), getResources().getString(R.string.no_network), Toast.LENGTH_SHORT).show();
           }
       });

        update_location.setOnClickListener(v13 -> {
            if (((ActivityMainList)requireActivity()).isOnline()) {
                if (((ActivityMainList)requireActivity()).isGPSEnabled()) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.update_loc), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(v13.getContext(), ActivityMainList.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    dismiss();
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.enable_gps), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getActivity(), getResources().getString(R.string.no_network), Toast.LENGTH_SHORT).show();
            }
        });

        return v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.AppTheme_FullScreenDialog);
    }

    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
            dialog.getWindow().setWindowAnimations(R.style.Fade);
        }
    }

}
