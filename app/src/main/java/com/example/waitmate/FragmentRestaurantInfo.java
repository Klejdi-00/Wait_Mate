package com.example.waitmate;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import java.util.Locale;

public class FragmentRestaurantInfo extends DialogFragment {

    Button update_wait,view_review,leave_review;
    TextView restaurant_name,restaurant_address,current_wait_time,rating;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d("Activity Lifecycle","onCreateView() Called");
        View v = inflater.inflate(R.layout.fragment_restaurant_info,container,false);

        update_wait = (Button) v.findViewById(R.id.update_wait_time);
        view_review = (Button) v.findViewById(R.id.view_reviews);
        leave_review = (Button) v.findViewById(R.id.leave_review);
        restaurant_name = (TextView)v.findViewById(R.id.restaurant_name);
        restaurant_address = (TextView)v.findViewById(R.id.restaurant_address);
        current_wait_time = (TextView)v.findViewById(R.id.current_wait_time);
        rating = (TextView)v.findViewById(R.id.rating);

        return v;
    }

    public static FragmentRestaurantInfo newInstance(String name, String address, int waitTime, double rating) {
        FragmentRestaurantInfo fragment = new FragmentRestaurantInfo();
        Bundle args = new Bundle();
        args.putString("address", address);
        args.putString("name", name);
        args.putInt("waitTime", waitTime);
        args.putDouble("rating", rating);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.AppTheme_FullScreenDialog);
    }

    @SuppressLint("SetTextI18n")
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
            dialog.getWindow().setWindowAnimations(R.style.Fade);
        }
        assert getArguments() != null;
        current_wait_time.setText(String.valueOf(getArguments().getInt("waitTime")) + " " + getResources().getString(R.string.minutes));
        restaurant_address.setText(getArguments().getString("address"));
        restaurant_name.setText((getArguments().getString("name")));
        String str = String.format(Locale.US,"%.1f", getArguments().getDouble("rating"));
        rating.setText(str + " " + getResources().getString(R.string.stars));

        update_wait.setOnClickListener(v -> {
            FragmentManager manager = requireActivity().getSupportFragmentManager();
            DialogFragment dialogFragment = FragmentUpdateWaitTime.newInstance(getArguments().getInt("waitTime"), getArguments().getString("address"));
            dialogFragment.show(manager, "Update Wait Time");
        });

        view_review.setOnClickListener(v -> {
            FragmentManager manager = requireActivity().getSupportFragmentManager();
            DialogFragment dialogFragment = FragmentViewReviews.newInstance(getArguments().getString("address"));
            if (((ActivityMainList)requireActivity()).isOnline()) {
                dialogFragment.show(manager, "View Reviews");
            } else {
                Toast.makeText(getActivity(), getResources().getString(R.string.no_network), Toast.LENGTH_SHORT).show();
            }
        });

        leave_review.setOnClickListener(v -> {
            FragmentManager manager = requireActivity().getSupportFragmentManager();
            SharedPreferences sharedPref = requireActivity().getSharedPreferences("UserID", Context.MODE_PRIVATE);
            String userName = sharedPref.getString("UID", "");
            DialogFragment dialogFragment = FragmentLeaveReview.newInstance(userName, getArguments().getString("address"));
            dialogFragment.show(manager, "Leave Review");
        });
    }
}