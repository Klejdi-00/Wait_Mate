package com.example.waitmate;

public interface FragmentCallback {
    void replaceFragment();
}
