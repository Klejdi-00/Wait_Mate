package com.example.waitmate;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Objects;

public class FragmentViewReviews extends DialogFragment {

    private static final String ADDRESS = "address";
    RecyclerView recyclerView;
    ReviewsRecyclerViewAdapter recyclerViewAdapter;
    ArrayList<ArrayList<Object>> rowsArrayList = new ArrayList<>();
    FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
    DatabaseReference mReference = mDatabase.getReference("Restaurant");
    boolean isLoading = false;
    String address;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d("Activity Lifecycle","onCreateView() Called");
        View v = inflater.inflate(R.layout.fragment_view_reviews,container,false);
        recyclerView = v.findViewById(R.id.recyclerView);

        populateData();

        return v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.AppTheme_FullScreenDialog);
    }

    static FragmentViewReviews newInstance(String address) {
        FragmentViewReviews f = new FragmentViewReviews();
        Bundle args = new Bundle();
        args.putString(ADDRESS, address);
        f.setArguments(args);
        return f;
    }

    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
            dialog.getWindow().setWindowAnimations(R.style.Fade);
        }
        if (getArguments() != null) {
            address = getArguments().getString(ADDRESS);
        }
    }

    private void populateData() {

        assert getArguments() != null;
        mReference.child(getArguments().getString(ADDRESS)).child("reviews").orderByValue().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                        String username = dataSnapshot.getKey();
                        assert username != null;
                        String date = dataSnapshot.child("date").getValue(String.class);
                        String review = dataSnapshot.child("text").getValue(String.class);
                        int rating = Integer.parseInt(Objects.requireNonNull(Objects.requireNonNull(dataSnapshot.child("rating").getValue()).toString()));
                        ArrayList<Object> list = new ArrayList<>();
                        list.add(username);
                        list.add(date);
                        list.add(review);
                        list.add(rating);
                        rowsArrayList.add(list);
                    }
                    initAdapter();
                    initScrollListener();
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getActivity(), getResources().getString(R.string.no_data), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initAdapter() {
        recyclerViewAdapter = new ReviewsRecyclerViewAdapter(rowsArrayList);
        recyclerView.setAdapter(recyclerViewAdapter);
    }

    private void initScrollListener() {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                                             @Override
                                             public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                                                 super.onScrollStateChanged(recyclerView, newState);
                                             }
                                             @Override
                                             public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                                                 super.onScrolled(recyclerView, dx, dy);

                                                 LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                                                 if (!isLoading) {
                                                     if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == rowsArrayList.size() - 1) {
                                                         // bottom of list!
                                                         loadMore();
                                                         isLoading = true;
                                                     }
                                                 }
                                             }
                                         }
        );
    }

    private void loadMore() {
        rowsArrayList.add(null);
        recyclerViewAdapter.notifyItemInserted(rowsArrayList.size() - 1);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                rowsArrayList.remove(rowsArrayList.size() - 1);
                int scrollPosition = rowsArrayList.size();
                recyclerViewAdapter.notifyItemRemoved(scrollPosition);
                int currentSize = scrollPosition;

                // Next load more option is to be shown after every 10 items.
                int nextLimit = currentSize + 10;

                while (currentSize < nextLimit && currentSize < rowsArrayList.size()) {
                   // rowsArrayList.add("Review " + currentSize);
                    currentSize++;
                }

                recyclerViewAdapter.notifyDataSetChanged();
                isLoading = false;
            }
        }, 2000);
    }

}