package com.example.waitmate;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;
import java.util.Objects;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    Activity activity;

    public List<String> mItemList;
    public RecyclerViewAdapter(List<String> itemList, Activity activity) {
        mItemList = itemList;
        this.activity = activity;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.mainlist_items, parent, false);
            return new ItemViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.mainlist_loading, parent, false);
            return new LoadingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        if (viewHolder instanceof ItemViewHolder) {

            populateItemRows((ItemViewHolder) viewHolder, position);
        } else if (viewHolder instanceof LoadingViewHolder) {
            showLoadingView((LoadingViewHolder) viewHolder, position);
        }

    }

    @Override
    public int getItemCount() {
        return mItemList == null ? 0 : mItemList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mItemList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }


    private static class ItemViewHolder extends RecyclerView.ViewHolder {

        ImageView resImage;
        TextView resName;
        TextView resAddress;
        TextView waitTime;
        ImageView ratingImage;
        Button update_wt;
        Button leave_rw;
        View[] resInfoViews = {null, null, null, null, null};

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            resName = itemView.findViewById(R.id.resName);
            resInfoViews[0] = resName;
            resAddress = itemView.findViewById(R.id.resAddress);
            resInfoViews[1] = resAddress;
            waitTime = itemView.findViewById(R.id.waitTime);
            resInfoViews[2] = waitTime;
            ratingImage = itemView.findViewById(R.id.rating);
            resInfoViews[3] = ratingImage;
            resImage = itemView.findViewById(R.id.resImage);
            resInfoViews[4] = ratingImage;
            update_wt = itemView.findViewById(R.id.item_update_wt);
            leave_rw = itemView.findViewById(R.id.item_leave_rv);
        }

    }

    private static class LoadingViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    private void showLoadingView(LoadingViewHolder viewHolder, int position) {
        //ProgressBar would be displayed

    }

    private void populateItemRows(ItemViewHolder viewHolder, int position) {
        char separator = ((char)007);
        String item = mItemList.get(position);

        // Getting the name from the string
        int separatorIndex = item.indexOf(separator);
        String name = item.substring(0,separatorIndex);
        String address = item.substring(separatorIndex + 1);
        viewHolder.resName.setText(name);
        viewHolder.resAddress.setText(address);

        address = address.replace("#","Nº");
        address = address.replace(".","");
        address = address.replace("$","");
        address = address.replace("[","");
        address = address.replace("]","");

        FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
        DatabaseReference mReference = mDatabase.getReference("Restaurant");
        String tempAddress = address;
        mReference.child(address).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    int waitTime = Objects.requireNonNull(dataSnapshot.child("waitTime").getValue(Integer.class));
                    viewHolder.waitTime.setText(String.valueOf(waitTime));

                    double rating = Objects.requireNonNull(dataSnapshot.child("rating").getValue(Double.class));
                    int id;
                    if (rating < 1.5) {
                        id = R.drawable.one_star;
                    } else if (rating < 2.5) {
                        id = R.drawable.two_stars;
                    } else if (rating < 3.5) {
                        id = R.drawable.three_stars;
                    } else if (rating < 4.5) {
                        id = R.drawable.four_stars;
                    } else {
                        id = R.drawable.five_stars;
                    }
                    viewHolder.ratingImage.setImageResource(id);

                    viewHolder.update_wt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            FragmentManager manager = ((AppCompatActivity)activity).getSupportFragmentManager();
                            DialogFragment dialog = FragmentUpdateWaitTime.newInstance(waitTime, tempAddress);
                            dialog.show(manager, "Update Wait Time");
                        }
                    });

                    viewHolder.leave_rw.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            FragmentManager manager = ((AppCompatActivity)activity).getSupportFragmentManager();
                            SharedPreferences sharedPref = activity.getSharedPreferences("UserID", Context.MODE_PRIVATE);
                            String userName = sharedPref.getString("UID", "");
                            DialogFragment dialog = FragmentLeaveReview.newInstance(userName, tempAddress);
                            dialog.show(manager, "Leave Review");
                        }
                    });

                    for (View v : viewHolder.resInfoViews) {
                        v.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                FragmentManager manager = ((AppCompatActivity)activity).getSupportFragmentManager();
                                DialogFragment dialog = FragmentRestaurantInfo.newInstance(name, tempAddress, waitTime, rating);
                                dialog.show(manager, "Restaurant Info");
                            }
                        });
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }
}
