package com.example.waitmate;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;


public class FragmentLeaveReview extends DialogFragment {

    private static final String USERNAME = "username";
    private static final String ADDRESS = "address";
    Button rating_one,rating_two,rating_three,rating_four,rating_five,add_review;
    EditText add_review_et;
    String username, address;
    int rating = 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d("Activity Lifecycle", "onCreateView() Called");
        View v = inflater.inflate(R.layout.fragment_leave_review,container,false);

        rating_one = (Button) v.findViewById(R.id.rating_one);
        rating_two = (Button) v.findViewById(R.id.rating_two);
        rating_three = (Button) v.findViewById(R.id.rating_three);
        rating_four = (Button) v.findViewById(R.id.rating_four);
        rating_five = (Button) v.findViewById(R.id.rating_five);
        add_review = (Button) v.findViewById(R.id.add_review);
        add_review_et = (EditText)v.findViewById(R.id.add_review_et);


        return v;
    }

    public static FragmentLeaveReview newInstance(String username, String address) {
        FragmentLeaveReview fragment = new FragmentLeaveReview();
        Bundle args = new Bundle();
        args.putString(ADDRESS, address);
        args.putString(USERNAME, username);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.AppTheme_FullScreenDialog);
    }

    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
            dialog.getWindow().setWindowAnimations(R.style.Fade);
        }
        if (getArguments() != null) {
            username = getArguments().getString(USERNAME);
            address = getArguments().getString(ADDRESS);
        }

        rating_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(v == rating_one) {
                    rating_one.setBackgroundResource(R.drawable.filled_star);
                    rating_two.setBackgroundResource(R.drawable.empty_star);
                    rating_three.setBackgroundResource(R.drawable.empty_star);
                    rating_four.setBackgroundResource(R.drawable.empty_star);
                    rating_five.setBackgroundResource(R.drawable.empty_star);
                }

                rating = 1;
            }
        });

        rating_two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(v == rating_two) {
                    rating_one.setBackgroundResource(R.drawable.filled_star);
                    rating_two.setBackgroundResource(R.drawable.filled_star);
                    rating_three.setBackgroundResource(R.drawable.empty_star);
                    rating_four.setBackgroundResource(R.drawable.empty_star);
                    rating_five.setBackgroundResource(R.drawable.empty_star);
                }

                rating = 2;
            }
        });

        rating_three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(v == rating_three) {
                    rating_one.setBackgroundResource(R.drawable.filled_star);
                    rating_two.setBackgroundResource(R.drawable.filled_star);
                    rating_three.setBackgroundResource(R.drawable.filled_star);
                    rating_four.setBackgroundResource(R.drawable.empty_star);
                    rating_five.setBackgroundResource(R.drawable.empty_star);
                }

                rating = 3;
            }
        });

        rating_four.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(v == rating_four) {
                    rating_one.setBackgroundResource(R.drawable.filled_star);
                    rating_two.setBackgroundResource(R.drawable.filled_star);
                    rating_three.setBackgroundResource(R.drawable.filled_star);
                    rating_four.setBackgroundResource(R.drawable.filled_star);
                    rating_five.setBackgroundResource(R.drawable.empty_star);
                }

                rating = 4;
            }
        });

        rating_five.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(v == rating_five) {
                    rating_one.setBackgroundResource(R.drawable.filled_star);
                    rating_two.setBackgroundResource(R.drawable.filled_star);
                    rating_three.setBackgroundResource(R.drawable.filled_star);
                    rating_four.setBackgroundResource(R.drawable.filled_star);
                    rating_five.setBackgroundResource(R.drawable.filled_star);
                }

                rating = 5;
            }
        });

        add_review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((ActivityMainList)requireActivity()).isOnline()) {
                    FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
                    DatabaseReference mReference = mDatabase.getReference("Restaurant");
                    if (add_review_et.getText().toString().length() > 0 && rating != 0) {
                        mReference.child(address).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot.exists()) {
                                    String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                                    String text = add_review_et.getText().toString();
                                    Review review = new Review(text, currentDate, rating);
                                    double averageRating = Double.parseDouble(Objects.requireNonNull(Objects.requireNonNull(dataSnapshot.child("rating").getValue()).toString()));
                                    if (!dataSnapshot.hasChild("reviews")) {
                                        mReference.child(address).child("rating").setValue(rating);
                                    } else {
                                        long size = dataSnapshot.child("reviews").getChildrenCount();
                                        if (dataSnapshot.child("reviews").hasChild(username)) {
                                            int currentRating = Integer.parseInt(Objects.requireNonNull(dataSnapshot.child("reviews").child(username).child("rating").getValue()).toString());
                                            long newTotal = Math.round(averageRating * size - currentRating + rating);
                                            averageRating = (double) newTotal / size;
                                        } else {
                                            averageRating = (averageRating * size + rating) / (size + 1);
                                        }
                                        mReference.child(address).child("rating").setValue(averageRating);
                                    }
                                    mReference.child(address).child("reviews").child(username).setValue(review);
                                    Toast.makeText(getActivity(), getResources().getString(R.string.review_add), Toast.LENGTH_SHORT).show();
                                    if (((ActivityMainList) requireActivity()).isGPSEnabled()) {
                                        Intent intent = new Intent(getContext(), ActivityMainList.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                    } else {
                                        Toast.makeText(getActivity(), getResources().getString(R.string.enable_gps), Toast.LENGTH_SHORT).show();
                                    }
                                    dismiss();
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                Toast.makeText(getActivity(), getResources().getString(R.string.failed_data) + databaseError, Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else {
                        Toast.makeText(getActivity(), getResources().getString(R.string.blank_ratingreview), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.no_network), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d("Activity Lifecycle","onDestroyView() Called");
    }


}