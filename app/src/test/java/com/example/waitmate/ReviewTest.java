package com.example.waitmate;

import static org.junit.Assert.*;

import org.junit.Test;

public class ReviewTest {

    @Test
    public void getText() {
        Review r = new Review("good", "2022-12-2", 4);
        assertEquals(r.getText(), "good");
    }

    @Test
    public void setText() {
        Review r = new Review("good", "2022-12-2", 4);
        r.setText("bad");
        assertEquals(r.getText(), "bad");
    }

    @Test
    public void getDate() {
        Review r = new Review("good", "2022-12-2", 4);
        assertEquals(r.getDate(), "2022-12-2");
    }

    @Test
    public void setDate() {
        Review r = new Review("good", "2022-12-2", 4);
        r.setDate("2022-12-3");
        assertEquals(r.getDate(), "2022-12-3");
    }

    @Test
    public void getRating() {
        Review r = new Review("good", "2022-12-2", 4);
        assertEquals(r.getRating(), 4);
    }

    @Test
    public void setRating() {
        Review r = new Review("good", "2022-12-2", 2);
        r.setRating(4);
        assertEquals(r.getRating(), 4);
    }

}