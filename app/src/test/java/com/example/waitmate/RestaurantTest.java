package com.example.waitmate;


import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RestaurantTest {
    @Test
    public void getResName() {
        Restaurant r = new Restaurant("Name", 0, 4.5);
        assertEquals(r.getResName(), "Name");
    }

    @Test
    public void setResName() {
        Restaurant r = new Restaurant("Name", 0, 4.5);
        r.setResName("NewName");
        assertEquals(r.getResName(), "NewName");
    }

    @Test
    public void getWaitTime() {
        Restaurant r = new Restaurant("Name", 0, 4.5);
        assertEquals(r.getWaitTime(), 0);
    }

    @Test
    public void setWaitTime() {
        Restaurant r = new Restaurant("Name", 0, 4.5);
        r.setWaitTime(10);
        assertEquals(r.getWaitTime(), 10);
    }

    @Test
    public void getRating() {
        Restaurant r = new Restaurant("Name", 0, 4.5);
        assertEquals(r.getRating(), 4.5, 0.01);
    }

    @Test
    public void setRating() {
        Restaurant r = new Restaurant("Name", 0, 4.5);
        r.setRating(2.5);
        assertEquals(r.getRating(), 2.5, 0.01);
    }


}