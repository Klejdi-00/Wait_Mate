package com.example.waitmate;

import static org.junit.Assert.*;

import org.junit.Test;

public class UserTest {
    @Test
    public void getPassword() {
        User u = new User("password", "12345");
        assertEquals(u.getPassword(), "password");
    }

    @Test
    public void setPassword() {
        User u = new User("password", "12345");
        u.setPassword("newPassword");
        assertEquals(u.getPassword(), "newPassword");
    }

    @Test
    public void getLocation() {
        User u = new User("password", "12345");
        assertEquals(u.getLocation(), "12345");
    }

    @Test
    public void setLocation() {
        User u = new User("password", "12345");
        u.setLocation("54321");
        assertEquals(u.getLocation(), "54321");
    }
}